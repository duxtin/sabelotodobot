const uso = `Uso: \n*—!ayuda:* \n    Muestra la ayuda.\n*—!puntajes:* \n    Muestra los puntajes de la ronda actual.\n*—PUNTO PARA FULANITO:*\n    le asigna un punto al jugador FULANITO.\n*—MEDIO PUNTO PARA MENGANITO:*\n    le asigna medio punto al jugador MENGANITO.\n*—!LIMPIAR PUNTAJES:*\n    limpia los puntajes y arranca nueva ronda.`;

const ayuda = (msg, comando) => {
    if (["!ayuda", "!help"].includes(comando.toLowerCase())) {
        msg.reply(uso);
    }
};

const obtenerJugador = msg => {
    const noEntendiText = 'No entendí a quién asignarle el punto. ¿Puedes repetir la orden?';
    const usuarioPunto = msg.mentions.users.first();
    const jugador = msg.guild.member(usuarioPunto);
    if (!usuarioPunto || !jugador) {
        msg.reply(noEntendiText);
        return;
    }
    return jugador;
}

const enviarPuntuacion = (msg, nuevaPuntuacion, indice) => msg.channel.send('Puntuación asignada a ' + msg.content.split(" ")[indice] + ', puntuación acumulada: ' + nuevaPuntuacion);

const asignaPuntoCompleto = function (msg, puntuacion) {
    const jugador = obtenerJugador(msg);
    if (!jugador) return;
    enviarPuntuacion(msg, puntuacion.darPunto(jugador), 2);
};

const asignaMedioPunto = function (msg, puntuacion) {
    const jugador = obtenerJugador(msg);
    if (!jugador) return;
    enviarPuntuacion(msg, puntuacion.darMedioPunto(jugador), 3);
};

const esTallador = msg => msg.member.roles.find(rol => rol.name == 'tallador');

const elegirAsignacionPuntos = msg => {
    const mensajeSplit = msg.content.split(" ");

    const tienePuntoText = indice => ["punto"].includes(mensajeSplit[indice].toLowerCase());
    const tieneMedioPuntoText = ["medio", "1/2", "0.5"].includes(mensajeSplit[0].toLowerCase());
    const tieneParaText = indice => ["para", "pa"].includes(mensajeSplit[indice].toLowerCase());

    if (tienePuntoText(0) && tieneParaText(1) && mensajeSplit[2]) {
        return asignaPuntoCompleto;
    }
    if (tieneMedioPuntoText && tienePuntoText(1) && tieneParaText(2) && mensajeSplit[3]) {
        return asignaMedioPunto;
    }
}

const procesar = (msg, puntuacion) => {
    if (esTallador(msg)) {
        const asignarPuntaje = elegirAsignacionPuntos(msg);
        if (asignarPuntaje) {
            asignarPuntaje(msg, puntuacion);
            ayuda(msg, msg.content.split(" ")[0]);
        }
    }
};

module.exports.procesar = procesar;
module.exports.esTallador = esTallador;