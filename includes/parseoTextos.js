/**
 * Array de textos para parseo de mensajes
 */
const puntajesText = ["!puntajes", "!puntaje", "!puntos", "!score", "!scoring"];
const puntoText = ["punto"];
const medioText = ["medio", "1/2", "0.5"];
const menosText = ["menos", "quitar", "restar"];
const paraText = ["para", "pa"];
const ayudaText = ["!ayuda", "!help"];
const nadieText = ["nadie", "nadies", "naiden", "naides"];
const reiniciarText = ["!LIMPIARPUNTAJES"];
const cancelarReiniciarText = ["!CANCELARLIMPIARPUNTAJES"];
const quejaText = ["!quejarse", "jueputa", "!queja", "exijo var", "VAR"];
const quejaRespuesta = ["Cállese, que esto es una dictalladura.", 
    "¡A quejarse al Mono de la pila!", 
    "🐸🐸🐸", 
    "Esta gente chilla más que una caja de pollos...",
    "Su queja ha sido recibida y remitida correctamente al departamento de trituración de papel.",
    "Se va a ganar su bananeada.",
    "A llorar a la llorería.",
    "Esto le quedó grande. Mejor vaya juegue Among Us.",
    "Las quejas las atiende @hlinares por Twitter, yo no sé.",
    "No me preguntes, solo soy un bot. **risitas**",
    "Si te preocupas mucho, te arrugas.",
    "Les juro que fue @anfeliz el que me dañó."
    ];
const ayudaRespuesta = `Uso: 
    *!ayuda:* 
        Muestra la ayuda.
    *!puntajes:* 
        Muestra los puntajes de la ronda actual.
    *!quejarse:* 
        Muestra la interfaz de PQRS.`;
const ayudaTallador = `       
    -- PARA EL TALLADOR --
    *PUNTO PARA FULANITO:*
        le asigna un punto al jugador FULANITO.
    *MEDIO PUNTO PARA MENGANITO:*
        le asigna medio punto al jugador MENGANITO.
    *MENOS PUNTO PARA NOALSILENCIO:*
        le quita un punto al jugador NOALSILENCIO. (Y así evitar que gane)
    *MEDIO PUNTO PARA PERENCEJO:*
        le asigna medio punto al jugador PERENCEJO.
    *!LIMPIARPUNTAJES:* (así, en mayúscula)
        limpia los puntajes y arranca nueva ronda.`;
const usuarioDesconocido = ["ole, no entendí a quién darle el punto. Repite por favor.",
    "¡¿qué?!",
    "hágame el favor de escribir bien.",
    "ese man no está jugando hoy, toca ponerle falta.",
    "usuario no reconocido: ¡sigue intentando!",
    "oiga, necesitamos que se ponga las pilas y empiece a tallar bien.",
    "creo que su teclado está en Francés.",
    "¡Hable más fuerte, que tengo una toalla!"
    ];
const puntajeVacioRespuesta = "¡No puede ser! ¡No hay puntos! 😱😱😱";

/**
 * Array de jugadores Map<User, int>
 */

let jugadores = new Map();
 
/**
 * Array para guardar el juego anterior, en caso de necesitar reiniciar.
 */
let jugadoresViejos = null;


/**
 * Función principal para determinar la naturaleza del texto y llamar a la función apropiada
 * @param {Message} msg Objeto del mensaje a parsear
 */
const analizarTexto = (msg) => {
    /**
     * Si es el bot, ignorar mensaje
     */
    if (msg.author.bot) {
        return;
    }

    const mensajeSplit = msg.content.split(" ");

    /**
     * Métodos para todos, excepto para el bot
     */
    if (puntajesText.indexOf(mensajeSplit[0].toLowerCase()) > -1) {
        msg.channel.send(cargarPuntajes());
        return;
    } 

    if (quejaText.indexOf(mensajeSplit[0].toLowerCase()) > -1) {
        msg.channel.send(publicarMensajeQueja(msg));
        return;
    } 

    if (ayudaText.indexOf(mensajeSplit[0]) > -1) {
        //TODO Verificar 
        /**
         * if (msg.member.roles.find(rol => rol.name == 'tallador')) msg.reply(ayudaRespuesta+ayudaTallador);
                          ^

            TypeError: Cannot read property 'roles' of null
         */
        if (msg.member != null && msg.member.roles.cache.some(rol => rol.name == 'tallador')) {
            msg.reply(ayudaRespuesta+ayudaTallador);
        } 
        else { 
            msg.reply(ayudaRespuesta);
        }
        return;
    }
    





    /**
     * Métodos exclusivos del tallador
     */
    if (msg.member.roles.cache.some(rol => rol.name == 'tallador')) {
        //Se verificó primero que es el tallador

        let usuarioPunto;
        let longitudMensaje = mensajeSplit.length;
        if (longitudMensaje >= 4
                && menosText.indexOf(mensajeSplit[0].toLowerCase()) > -1
                && medioText.indexOf(mensajeSplit[1].toLowerCase()) > -1 
                && puntoText.indexOf(mensajeSplit[2].toLowerCase()) > -1 
                && paraText.indexOf(mensajeSplit[3].toLowerCase()) > -1) {
            //Es el caso de QUITAR medio punto
            jugadorPunto = obtenerJugadorDelTexto(msg, mensajeSplit[4]);
            if (jugadorPunto != null) {
                sumarPuntaje(jugadorPunto, -0.5);
                msg.channel.send("Le quité medio punto a "+jugadorPunto.username+". ¡Qué estafa!")
            } 
            else (publicarMensajeUsuarioDesconocido(msg));
            return;
        }
        if (longitudMensaje >= 3
                && menosText.indexOf(mensajeSplit[0].toLowerCase()) > -1 
                && puntoText.indexOf(mensajeSplit[1].toLowerCase()) > -1 
                && paraText.indexOf(mensajeSplit[2].toLowerCase()) > -1) {
            //Es el caso de QUITAR punto
            jugadorPunto = obtenerJugadorDelTexto(msg, mensajeSplit[3]);
            if (jugadorPunto != null) {
                sumarPuntaje(jugadorPunto, -1);
                msg.channel.send("Le quité UN punto a "+jugadorPunto.username+". ¡Qué robo tan descarado!")
            }
            else (publicarMensajeUsuarioDesconocido(msg));
            return;
        }
        if (longitudMensaje >= 3
                && medioText.indexOf(mensajeSplit[0].toLowerCase()) > -1 
                && puntoText.indexOf(mensajeSplit[1].toLowerCase()) > -1 
                && paraText.indexOf(mensajeSplit[2].toLowerCase()) > -1) {
            //Es el caso de medio punto
            jugadorPunto = obtenerJugadorDelTexto(msg, mensajeSplit[3]);
            if (jugadorPunto != null) {
                sumarPuntaje(jugadorPunto, 0.5);
                msg.channel.send("Medio puntazo asignado a "+jugadorPunto.username)
            }
            else (publicarMensajeUsuarioDesconocido(msg));
            return;
        }
        if (longitudMensaje >= 2
                && puntoText.indexOf(mensajeSplit[0].toLowerCase()) > -1 
                && paraText.indexOf(mensajeSplit[1].toLowerCase()) > -1) {
            //Es el caso de punto
            jugadorPunto = obtenerJugadorDelTexto(msg, mensajeSplit[2]);
            if (jugadorPunto != null) {
                sumarPuntaje(jugadorPunto, 1);
                msg.channel.send("Punto asignado a "+jugadorPunto.username)
            }
            else (publicarMensajeUsuarioDesconocido(msg));
            return;
        }

        if (longitudMensaje >= 1 
            && puntoText.indexOf(mensajeSplit[0].toLowerCase()) > -1) {
            publicarMensajeUsuarioDesconocido(msg);
            return;
        }
    }






    /**
     * Método para reiniciar puntajes, llamado por tallador o admin
     */
    if (msg.member.roles.cache.some(rol => rol.name == 'tallador') || msg.member.roles.cache.some(rol => rol.name == 'administrador-interino') || msg.member.roles.cache.some(rol => rol.name == 'Admin')) {

        /**
         * Listener para reiniciar puntajes
         */

        if (reiniciarText.indexOf(msg.content) > -1) {
            msg.reply("Se reiniciarán los puntajes.");
            msg.channel.send(cargarPuntajes());
            msg.channel.send("¡Ha arrancado una nueva ronda!");
            jugadoresViejos = jugadores;
            jugadores = new Map();

            return;
        }
    }

    if (msg.member.roles.cache.some(rol => rol.name == 'administrador-interino') || msg.member.roles.cache.some(rol => rol.name == 'Admin')) {

        /**
         * Listener para cancelar reinicio de puntaje
         */

        if (cancelarReiniciarText.indexOf(msg.content) > -1 && jugadoresViejos != null) {
            msg.channel.send("*Cancelando el reinicio de los puntajes*");
            jugadores = jugadoresViejos;
            msg.channel.send(cargarPuntajes());

            return;
        }
    }
}

/**
 * Función que permite obtener un usuario del canal a partir del texto enviado en el llamado a asignación de puntos.
 * Por ejemplo, "Punto para XYZ" obtendría el usuario XYZ.
 * @param {Message} msg Objeto de mensaje del que se obtiene el channel
 */
const obtenerJugadorDelTexto = (msg, textoUsuario) => {
    
    //Validar que hayan escrito un usuario
    if (msg == null || textoUsuario == null || textoUsuario == "") return null;
    
    let usuario = null;

    //Verificar primero si es punto para nadie
    if (nadieText.indexOf(textoUsuario.toLowerCase()) > -1) {
        textoUsuario = "SabelotodoBot";
    }

    //Se itera el listado de miembros del canal
    msg.channel.members.forEach(guildMember => {
        if (
            guildMember.user.username.toLowerCase() == textoUsuario.toLowerCase()
            || (guildMember.nickname != null && guildMember.nickname.toLowerCase() == textoUsuario.toLowerCase())
        ) {
            usuario = guildMember.user;
        }
    });
    //Si en este punto el usuario sigue siendo nulo, toca buscarlo como mención
    if (usuario == null) {
        usuario = msg.mentions.users.values().next().value;
    }

    return usuario;
    
}


/**
 * Método que suma el puntaje del usuario al mapa de puntajes.
 * @param {User} usuarioPunto Usuario al que se le sumará el punto
 * @param {int} puntajeASumar Puntaje a sumarle al usuario
 */

 const sumarPuntaje = (jugadorPunto, puntajeASumar) => {
    //Si el mapa de jugadores no tiene el jugador, añadirlo con el puntaje respectivo
    if(!jugadores.has(jugadorPunto)) {
        jugadores.set(jugadorPunto, puntajeASumar);
    }
    else {
        jugadores.set(jugadorPunto, jugadores.get(jugadorPunto)+puntajeASumar);
    }
    
    //console.log ("Puntaje del jugador "+ jugadorPunto.username +": "+jugadores.get(jugadorPunto));
    
    console.log("-------------------------------------------");
    console.log(cargarPuntajes());
}
 


/**
 * Método que retorna el string a ser puesto como mensaje por el bot con el consolidado de puntajes.
 */
const cargarPuntajes = () => {
    respuestaPuntajes = "Los puntajes:\n";
    jugadores.forEach((puntaje, jugadorPunto)  => {
        respuestaPuntajes += "\t\t";
        if (jugadorPunto.username == "SabelotodoBot") respuestaPuntajes += "SabelotodoBot (o sea NADIE)" + "\t\t\t";
        else respuestaPuntajes += jugadorPunto.username + "\t\t\t";
        respuestaPuntajes += puntaje + "\n"
    });
    if(jugadores.size == 0) respuestaPuntajes += "\t\t"+ puntajeVacioRespuesta;
    return respuestaPuntajes;
}

/**
 * Método que retorna el string a ser puesto como mensaje por el bot con el consolidado de puntajes.
 */
const cargarPuntajesJSON = () => {
    respuestaPuntajes = "Los puntajes:\n";
    jugadores.forEach((puntaje, jugadorPunto)  => {
        respuestaPuntajes += "\t\t";
        if (jugadorPunto.username == "SabelotodoBot") respuestaPuntajes += "SabelotodoBot (o sea NADIE)" + "\t\t\t";
        else respuestaPuntajes += jugadorPunto.username + "\t\t\t";
        respuestaPuntajes += puntaje + "\n"
    });
    if(jugadores.size == 0) respuestaPuntajes += "\t\t"+ puntajeVacioRespuesta;
    return respuestaPuntajes;
}

/**
 * Método con el que el bot responde cuando no entiende a quién asignarle el punto
 */
const publicarMensajeUsuarioDesconocido = (msg) => {
    msg.reply(usuarioDesconocido[Math.floor(Math.random()*usuarioDesconocido.length)]);
}

/**
 * Método con el que el bot responde cuando no entiende a quién asignarle el punto
 */
const publicarMensajeQueja = (msg) => {
    msg.reply(quejaRespuesta[Math.floor(Math.random()*quejaRespuesta.length)]);
}


module.exports = { analizarTexto };