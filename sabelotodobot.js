require('dotenv').config();
const { Client, Events, GatewayIntentBits } = require('discord.js');

//const Discord = require('discord.js');
//const client = new Discord.Client();
const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent] });

const textos = require('./includes/parseoTextos.js');


client.on(Events.ClientReady, () => {
  console.log(`Sesión iniciada: ${client.user.tag}!`);
  jugadores = new Array();
});

/**
 * Deprecated
 */
/*client.on('message', msg => {
    console.log("MENSAJE: " + msg.content)
    textos.analizarTexto(msg);
});/**/

client.on(Events.MessageCreate, msg => {
  console.log ("Message received: " + msg.toString());
  textos.analizarTexto(msg);
});

client.on(Events.GuildMemberAdd, member => {
    member.send('¡Hola! ¡Bienvenido/a al Sabelotodo! Si no habías jugado antes, mira el mensaje anclado (pinned message) para que te enteres de las reglas. Utiliza el comando _!ayuda_ para ver para qué sirvo, o _!puntajes_ para consultar los puntajes. ');
});


client.login(process.env.BOT_TOKEN);
